#include "FileWordCounter.h"
#include <iostream>
#include <thread>
#include <vector>



int main(int argc, char **argv)
{
    if(argc < 2)
    {
        std::cout << "Please pass file path!" << std::endl;
        return -1;
    }

    std::unique_ptr<FileWordCounter> wordCounter(new FileWordCounter(std::unique_ptr<MmapFileReader>(new MmapFileReader()), argv[1]));
    auto num = wordCounter->NumberOfUniqueWords();
    std::cout << "Number of unique words: " << std::to_string(num) << std::endl;
}
