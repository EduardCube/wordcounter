#pragma once
#include <string>
#include <unordered_map>
#include <memory>
#include <mutex>
#include <fstream>
#include "MmapFileReader.h"

using Long = unsigned long long;

class IfstreamWrapper
{
public:
    virtual ~IfstreamWrapper() = default;
    virtual void open(const std::string& path);
    virtual void seekg(Long offset, std::ios_base::seekdir flag);
    virtual void close();
    virtual void read(char* buffer, Long size);

private:
    std::ifstream file;
};

class FileWordCounter
{ 
public:
    FileWordCounter(std::unique_ptr<MmapFileReader> fileReader, const std::string &path);
    ~FileWordCounter();
    long NumberOfUniqueWords();

protected:
    using WordCounter = std::unordered_map<std::string, bool>;
    char* ReadNextChank(Long& bufferSize, Long& fileOffset);
    Long CalculateLastWordOffset(IfstreamWrapper& stream, Long offset) const;
    void SetSizeLeftToRead(const Long size);

private:
    void Worker(Long& offset, WordCounter& words);
    bool PrcoessFileSingleThread();
    void PrcoessFileMultiThreaded();
    void CalculateBufferSize();
    bool AllignBufferSize(Long& bufferSize, Long offset) const;
    
    std::unique_ptr<WordCounter> m_wordCounter;
    std::unique_ptr<MmapFileReader> m_fileReader;
    Long m_fileSize;
    Long m_sizeLeftToRead;
    Long m_chankSize;
    std::mutex m_readMutex;
    std::string m_filePath;
};