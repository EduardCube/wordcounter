#include "FileWordCounter.h"
#include <vector>
#include <iostream>
#include <thread>
#include <cmath>
#include <unistd.h>
#include <chrono>
#include <sys/sysinfo.h>
#include "Logger.h"
#include "WordCounterHelper.h"
using namespace std::chrono;

void IfstreamWrapper::open(const std::string& path)
{
    file.open(path);
}

void IfstreamWrapper::seekg(Long offset, std::ios_base::seekdir flag)
{
    file.seekg(offset, flag);
}

void IfstreamWrapper::close()
{
    file.close();
}

void IfstreamWrapper::read(char* buffer, Long size)
{
    file.read(buffer, size);
}

namespace {
    const Long PAGE_SIZE = sysconf(_SC_PAGE_SIZE); 
    constexpr Long NUMBER_OF_ELEMENTS = 1000000;
    constexpr short PERCENTAGE_FROM_FREE_MEMORY = 80;
    
    Long getNumberOfFreeMemory()
    {
        struct sysinfo info;
        sysinfo(&info);
        return (info.freeram * PERCENTAGE_FROM_FREE_MEMORY) / 100;
    }

    Long RoundToPageSize(Long size)
    {
        return std::floor(size / PAGE_SIZE) * PAGE_SIZE;
    }

}

FileWordCounter::FileWordCounter(std::unique_ptr<MmapFileReader> fileReader, const std::string &path)
    : m_fileSize(0)
    , m_chankSize(PAGE_SIZE)
    , m_filePath(path)
    , m_sizeLeftToRead(0)
    , m_wordCounter(new WordCounter())
    , m_fileReader(std::move(fileReader))
{
    m_wordCounter->reserve(NUMBER_OF_ELEMENTS); 
}

FileWordCounter::~FileWordCounter()
{

}

// while using mmap we should read by chanks big files and each chank should be 
// divisible by page size, and can happen situation when chank would not cover last word completely
// so we should not separate word by different threads and read chanks whe ech chanks 
// contains complete last word till the end.
// so this function find the ending of last word and adds this extra size to buffer size
Long FileWordCounter::CalculateLastWordOffset(IfstreamWrapper& stream, Long offset) const
{
	try
    {
        stream.open(m_filePath);
    }
    catch( ...)
	{
        Logger::LogError("Uh oh, " + m_filePath + " could not be opened for reading!");
        return -1;
	}

    short bufferSize = 20;
    if(m_fileSize - offset < bufferSize)
    {
        bufferSize = m_fileSize - offset;
    }

    Long offsetCounter = 0;
    char buffer[bufferSize];
    bool run = true;

    while(run)
    {
        stream.seekg(offset, std::ios::beg);
        stream.read(buffer, bufferSize);
        int i = 0;

        for(int i = 0; i < bufferSize; ++i, ++offsetCounter)
        {
            if( WordCounterHelper::separators.find(buffer[i]) != WordCounterHelper::separators.end())
            {
                run = false;
                break;
            }
        }

        offset += bufferSize;
    }

    stream.close();
    return offsetCounter;
}

char* FileWordCounter::ReadNextChank(Long& bufferSize, Long& fileOffset)
{
    std::lock_guard lock(m_readMutex);
    if(m_sizeLeftToRead <= 0) 
    {
        return nullptr;
    }

    if(m_sizeLeftToRead < m_chankSize)
    {
        bufferSize = m_sizeLeftToRead;
        m_sizeLeftToRead=0;     
    }
    else
    {
        m_sizeLeftToRead -= m_chankSize;
    }

    std::cout << "offset: " << fileOffset  << " numberOfBytesToRead "  << bufferSize << std::endl;
    char* buffer = m_fileReader->Map(bufferSize, fileOffset);
    fileOffset += m_chankSize;
    
    return buffer;
}


bool FileWordCounter::AllignBufferSize(Long& bufferSize, Long offset) const
{
    Long fileOffset = offset + bufferSize;

    if(m_fileSize != fileOffset)
    {
        static IfstreamWrapper stream;
        Long extraWordOffset = CalculateLastWordOffset(stream, fileOffset);
        if(extraWordOffset == -1)
        {
            return false;
        }
        bufferSize += extraWordOffset;
    }

    return true;
}

void FileWordCounter::Worker(Long& offset, WordCounter& words )
{
    while(true)
    {
        Long bufferSize = m_chankSize;
        if(!AllignBufferSize(bufferSize, offset))
        {
            return;
        }

        char* buffer = ReadNextChank(bufferSize, offset);
        if(!buffer)
        {
            return;
        }
        WordCounterHelper::ProcessBuffer(words, buffer, bufferSize);
        if(!m_fileReader->UnMap(buffer, bufferSize))
        {
            return;
        }
    }
}

void FileWordCounter::PrcoessFileMultiThreaded()
{
    const uint32_t num_threads = std::thread::hardware_concurrency();
    std::vector<WordCounter> words(num_threads);
    std::vector<std::thread> pool;
    pool.reserve(num_threads);
    Long offset = 0;
    CalculateBufferSize();

    for(uint32_t i = 0; i < num_threads; ++i)
    {
        words.emplace_back(NUMBER_OF_ELEMENTS/num_threads);
        pool.push_back(std::thread(&FileWordCounter::Worker, this, std::ref(offset), std::ref(words[i])));
    }
    for(uint32_t i = 0; i < num_threads; ++i)
    {
        pool[i].join();
    }

    for(int i = 0; i < num_threads; ++i)
    {
       m_wordCounter->insert(words[i].begin(), words[i].end());
    }
}

bool FileWordCounter::PrcoessFileSingleThread()
{
    Long offset = 0;
    CalculateBufferSize();
    m_sizeLeftToRead = m_fileSize;

    while(m_sizeLeftToRead > 0)
    {
        std::cout << "m_sizeLeftToRead: " << m_sizeLeftToRead << std::endl;;
        Long bufferSize = m_chankSize;
        if(!AllignBufferSize(bufferSize, offset))
        {
            return false;
        }

        if(m_sizeLeftToRead < m_chankSize)
        {
            bufferSize = m_sizeLeftToRead;
            m_sizeLeftToRead=0;     
        } 
        else
        {
            m_sizeLeftToRead -= m_chankSize;
        }

        char* buffer = m_fileReader->Map(bufferSize, offset);
        if(!buffer)
        {
            return false;
        }
 
        std::cout << "offset: " << offset  << " numberOfBytesToRead "  << bufferSize << std::endl;

        WordCounterHelper::ProcessBuffer(*m_wordCounter.get(), buffer, bufferSize);

        if(!m_fileReader->UnMap(buffer, bufferSize))
        {
            return false;
        }

        offset += m_chankSize;
    }

    return true;

}

void FileWordCounter::CalculateBufferSize()
{
    Long freeMemory = getNumberOfFreeMemory();

    if(m_fileSize <= PAGE_SIZE)
    { // in this case we will calculate everything in one thread because thre is no sense to make several threads
        m_chankSize = m_fileSize;
    }
    else
    {   // here we calculating optimal buffer size relying on free memory and file size
        // if file size in less than size of free memory than we separate this file in one read using number of available threads
        // if file size is greater than size of free memory than we use whole memory for reading between threads
        // but for that we also should pick up fuffer size whick will be which will be divided without remainder on PAGE_SZIE
        // Because mmap uses offset which should be divided without remainder on PAGE_SZIE
        std::cout << "threads number: " << std::thread::hardware_concurrency() << std::endl;
        Long base = m_fileSize < freeMemory ? m_fileSize :freeMemory;
        Long chank = base / std::thread::hardware_concurrency();
        m_chankSize = RoundToPageSize(chank);
    }
}

long FileWordCounter::NumberOfUniqueWords()
{
    if(!m_fileReader->Open(m_filePath, m_fileSize))
    {
        return -1;
    }
    m_sizeLeftToRead = m_fileSize;

    std::cout << "Free memory in mb: " << getNumberOfFreeMemory()/1000000 << std::endl;

    auto start = high_resolution_clock::now();

    PrcoessFileMultiThreaded();

    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);

    std::cout << "Duration in sec:" << duration.count() /  1000000 << std::endl;;

    return m_wordCounter->size();
}

void FileWordCounter::SetSizeLeftToRead(const Long size)
{
    m_sizeLeftToRead = size;
}


