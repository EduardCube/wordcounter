#pragma once
#include <string>
using Long = unsigned long long;

class MmapFileReader
{
    
public:
    MmapFileReader();
    virtual ~MmapFileReader();
    virtual bool Open(const std::string& filePath, Long& fileSize);
    virtual char* Map(Long numberOfBytes, Long offset) const;
    virtual bool UnMap(char* buffer, Long numberOfBytes) const;
private:
    int m_fd;
};