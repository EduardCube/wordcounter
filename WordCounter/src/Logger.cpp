#include "Logger.h"
#include <iostream>
#include <string.h>

bool Logger::LogError(const std::string error)
{
    std::cout << error << " errno: " << strerror(errno) << std::endl;
    return false;
}

bool Logger::Log(const std::string error)
{
    std::cout << error << std::endl;
    return false;
}