#include "WordCounterHelper.h"
#include <unistd.h>
#include <ctype.h>
#include <iostream>

namespace
{

std::string ExtractWord(const char* str, WordCounterHelper::Long& startIndex, WordCounterHelper::Long bufferSize)
{
    using namespace WordCounterHelper;
    Long length = 0;

    for (int i = startIndex ; separators.find(str[i]) == separators.end() && i < bufferSize; ++i)
    {
        ++length;
    }
    
    return length == 0 ? "" : std::string(&str[startIndex], &str[startIndex] + length);
}

WordCounterHelper::Long CalculateStartIndex(const char* str)
{
    using namespace WordCounterHelper;
    static bool firstRead = true;
    Long startIndex = 0;

    if(!firstRead)
    {
        for(startIndex ; separators.find(str[startIndex]) == separators.end(); ++startIndex)
        {
        }
    }
    for (startIndex ; separators.find(str[startIndex]) != separators.end(); ++startIndex)
    {
    }

    firstRead = false;
    return startIndex;
}

}

void WordCounterHelper::ProcessBuffer( WordCounter& words, const char* buffer, Long bufferSize)
{
    Long index = CalculateStartIndex(buffer);

    for(index; index < bufferSize; ++index)
    {
        auto word = ExtractWord(buffer, index, bufferSize);
        words[word]=true;
        index += word.size();
    }

    words.erase("");
}
