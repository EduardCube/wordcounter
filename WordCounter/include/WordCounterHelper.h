#pragma once
#include <string>
#include <unordered_map>

namespace WordCounterHelper
{
    const std::unordered_map<char, bool> separators = {
    {' ', true}, {',', true}, {'.', true},
    {'!', true}, {'?', true}, {':', true},
    {'\n', true}, {'\0', true}, {';', true},
    {'\r', true}};

    using Long = unsigned long long;
    using WordCounter = std::unordered_map<std::string, bool>;
    void ProcessBuffer( WordCounter& words, const char* buffer, Long bufferSize);
}