cmake_minimum_required(VERSION 3.20)
PROJECT(CounterUnitTests)

set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_STANDARD 17)

include_directories(../include/)
link_directories(../lib/)

add_compile_options(-std=c++17)

set ( CMAKE_C_FLAGS "-Wall")

find_package(GTest REQUIRED)
include_directories(${GTEST_INCLUDE_DIRS})

# .cpp files
list(APPEND SOURCE_FILES
    main.cpp
)

SET(-DCMAKE_DEPENDS_USE_COMPILER=FALSE)

add_executable(CounterUnitTests ${SOURCE_FILES})


target_link_libraries(CounterUnitTests Counter gtest gmock)