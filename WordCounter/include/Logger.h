#pragma once
#include <string>

namespace Logger
{
    bool LogError(const std::string error);
    bool Log(const std::string error);
}