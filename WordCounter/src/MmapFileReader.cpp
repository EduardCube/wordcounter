#include "MmapFileReader.h"
#include "Logger.h"
#include <unistd.h>
#include <fstream>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
#include <sys/mman.h>
#include <memory>

MmapFileReader::MmapFileReader()
    : m_fd(0)
{

}

MmapFileReader::~MmapFileReader()
{
    if(m_fd)
    {
        close(m_fd);
    }
}

bool MmapFileReader::Open(const std::string& filePath, Long& fileSize)
{
    if(m_fd)
    {
        close(m_fd);
    }

    bool closeFD = true;
    m_fd = open64(filePath.c_str(), O_RDONLY);
    auto deleter = [&closeFD](int* f) { if(*f) {if(closeFD)close(*f);} delete f; };
    std::unique_ptr<int, decltype(deleter)> fileDescriptor(new int(m_fd), deleter);
    
    if (m_fd == -1)
    {
        return Logger::LogError("Error opening file for writing");
    }        
    
    struct stat fileInfo = {0};
    
    if (fstat(m_fd, &fileInfo) == -1)
    {
        Logger::LogError("Failed to get the file info");
    }
    
    if (fileInfo.st_size == 0)
    {
        return Logger::Log("Error: File is empty, nothing to do");
    }

    fileSize = fileInfo.st_size;
    closeFD = false;
    std::cout << "File size is " << std::to_string(fileSize) << std::endl;
    return true;
}

char* MmapFileReader::Map(Long numberOfBytes, Long offset) const
{
    char *map = (char*)mmap64(0, numberOfBytes, PROT_READ, MAP_PRIVATE , m_fd, offset);
    if (map == MAP_FAILED)
    {
        Logger::LogError("Failed to map the file");
        return nullptr;
    }
    
    return map;
}

bool MmapFileReader::UnMap(char* buffer, Long numberOfBytes) const
{
    if((munmap(buffer, numberOfBytes))==-1)
    {
        return Logger::LogError("Error Unmmapping the file");
    }

    return true;
}