#pragma once
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "MmapFileReader.h"
#include "FileWordCounter.h"
#include "WordCounterHelper.h"

using ::testing::_;
using ::testing::Return;

const std::string buffer("  Lorem ipsum dolor sit amet, consectetur adipiscing elit."
"Maecenas eu tempus massa, a tempor felis."
 "Morbi vitae faucibus erat. Nam auctor leo ac arcu varius, "
 "fringilla mollis diam tristique. Vivamus mauris risus,"
  "rhoncus et lorem sed, semper vestibulum elit."
   "Sed tincidunt purus ipsum, ac dictum augue suscipit eu."
    "Suspendisse at dignissim nisl, ut viverra quam. ");

char buffer2[] ="Lorem ipsumdolor sit amet";
constexpr unsigned offset = 5;
constexpr unsigned numberOfUniqueueWords = 51;

class MockIfstreamWrapper : public IfstreamWrapper
{
public:
  MOCK_METHOD(void, open, (const std::string&), (override));
  MOCK_METHOD(void, seekg, (Long offset, std::ios_base::seekdir flag), (override));
  MOCK_METHOD(void, close, (), (override));
  MOCK_METHOD(void, read, (char* buffer, Long size), (override));
};

class MockMmapFileReader : public MmapFileReader {
public:
  MOCK_METHOD(bool, Open, (const std::string& , Long& ), (override));
  MOCK_METHOD(char*, Map, (Long , Long ), (const, override));
  MOCK_METHOD(bool, UnMap, (char* , Long ), (const, override));
};

class TestFileWordCounter: public FileWordCounter
{
public:
  TestFileWordCounter(std::unique_ptr<MmapFileReader> fileReader)
  : FileWordCounter(std::move(fileReader), "")
  {}
  char* TestReadNextChank(Long szeLeftToRead)
  {
    SetSizeLeftToRead(szeLeftToRead);
    Long bufferSize; Long fileOffset;
    return ReadNextChank(bufferSize, fileOffset);
  }

  Long CalculateLastWordOffset(IfstreamWrapper& stream, Long offset) const
  {
    return FileWordCounter::CalculateLastWordOffset(stream, offset);
  }

};

struct TestFileWordCounterWrapper : public testing::Test
{
  void SetUp()
  {
    fileReader = new MockMmapFileReader();
    wordCounter = std::unique_ptr<TestFileWordCounter>
    (new TestFileWordCounter(std::unique_ptr<MockMmapFileReader>(fileReader)));
  }
  MockMmapFileReader* fileReader;
  std::unique_ptr<TestFileWordCounter> wordCounter;
};

TEST_F(TestFileWordCounterWrapper, ReadEmptyNextChank)
{
  EXPECT_TRUE(wordCounter->TestReadNextChank(0) == nullptr);
}

TEST_F(TestFileWordCounterWrapper, ReadNotEmptyNextChank)
{
  EXPECT_CALL(*fileReader, Map(_,_))
  .Times(1)
  .WillOnce(Return(const_cast<char*>("Lorem ipsum")));

  EXPECT_TRUE(wordCounter->TestReadNextChank(10) != nullptr);
}

TEST_F(TestFileWordCounterWrapper, CalculateLastWordOffset)
{
  MockIfstreamWrapper fileReader;

  EXPECT_CALL(fileReader, open(_))
  .Times(1);

  EXPECT_CALL(fileReader, seekg(_, _))
  .Times(1);

  EXPECT_CALL(fileReader, read(_,_))
  .Times(1)
  .WillOnce(testing::SetArrayArgument<0>(buffer2, buffer2 + strlen(buffer2) + 1));

  EXPECT_CALL(fileReader, close())
  .Times(1);

  EXPECT_TRUE(wordCounter->CalculateLastWordOffset(fileReader, 1) == offset);
}

TEST(CounterHelper, EmptyBuffer) {
  std::unordered_map<std::string, bool> words;
  WordCounterHelper::ProcessBuffer(words, "", 0);

  EXPECT_TRUE(words.size() == 0);     
}

TEST(CounterHelper, CorrectNumberOfUniqueWords) {
  std::unordered_map<std::string, bool> words;
  WordCounterHelper::ProcessBuffer(words, buffer.c_str(), buffer.size());

  EXPECT_TRUE(words.size() == numberOfUniqueueWords);      
}